# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kirigami-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kirigami-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-01 00:44+0000\n"
"PO-Revision-Date: 2023-07-13 00:52+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: aplicaçlões\n"

#: components/AbstractMaximizeComponent.qml:125
#, kde-format
msgid "Close"
msgstr "Fechar"

#: components/AlbumMaximizeComponent.qml:115
#, kde-format
msgid "Zoom in"
msgstr "Ampliar"

#: components/AlbumMaximizeComponent.qml:120
#, kde-format
msgid "Zoom out"
msgstr "Reduzir"

#: components/AlbumMaximizeComponent.qml:126
#, kde-format
msgid "Rotate left"
msgstr "Rodar para a esquerda"

#: components/AlbumMaximizeComponent.qml:132
#, kde-format
msgid "Rotate right"
msgstr "Rodar para a direita"

#: components/AlbumMaximizeComponent.qml:137
#, kde-format
msgctxt "@action:intoolbar"
msgid "Show caption"
msgstr "Mostrar a legenda"

#: components/AlbumMaximizeComponent.qml:137
#, kde-format
msgctxt "@action:intoolbar"
msgid "Hide caption"
msgstr "Esconder a legenda"

#: components/AlbumMaximizeComponent.qml:143
#, kde-format
msgid "Save as"
msgstr "Gravar como"

#: components/AlbumMaximizeComponent.qml:202
#, kde-format
msgid "Previous image"
msgstr "Imagem anterior"

#: components/AlbumMaximizeComponent.qml:220
#, kde-format
msgid "Next image"
msgstr "Imagem seguinte"

#: components/Banner.qml:186
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr "Fechar"

#: components/DownloadAction.qml:37
#, kde-format
msgid "Download"
msgstr "Obter"

#: components/VideoMaximizeDelegate.qml:241
#, kde-format
msgctxt "@action:button"
msgid "Volume"
msgstr "Volume"

#: dateandtime/DatePicker.qml:141
#, kde-format
msgctxt "%1 is month name, %2 is year"
msgid "%1 %2"
msgstr "%1 de %2"

#: dateandtime/DatePicker.qml:182
#, fuzzy, kde-format
#| msgctxt "kirigami-addons"
#| msgid "Days"
msgctxt "@title:tab"
msgid "Days"
msgstr "Dias"

#: dateandtime/DatePicker.qml:190
#, fuzzy, kde-format
#| msgctxt "kirigami-addons"
#| msgid "Months"
msgctxt "@title:tab"
msgid "Months"
msgstr "Meses"

#: dateandtime/DatePicker.qml:196
#, fuzzy, kde-format
#| msgctxt "kirigami-addons"
#| msgid "Years"
msgctxt "@title:tab"
msgid "Years"
msgstr "Anos"

#: dateandtime/DatePopup.qml:55
#, kde-format
msgid "Previous"
msgstr "Anterior"

#: dateandtime/DatePopup.qml:64
#, kde-format
msgid "Today"
msgstr "Hoje"

#: dateandtime/DatePopup.qml:73
#, kde-format
msgid "Next"
msgstr "Seguinte"

#: dateandtime/DatePopup.qml:92
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: dateandtime/DatePopup.qml:101
#, kde-format
msgid "Accept"
msgstr "Aceitar"

#: dateandtime/private/TumblerTimePicker.qml:88
#, kde-format
msgctxt "Time separator"
msgid ":"
msgstr ":"

#: formcard/AboutKDE.qml:23 formcard/labs/AboutKDE.qml:28
#, kde-format
msgid "About KDE"
msgstr "Acerca do KDE"

#: formcard/AboutKDE.qml:50 formcard/labs/AboutKDE.qml:60
#, kde-format
msgid "KDE"
msgstr "KDE"

#: formcard/AboutKDE.qml:59 formcard/labs/AboutKDE.qml:69
#, kde-format
msgid "Be Free!"
msgstr "Seja Livre!"

#: formcard/AboutKDE.qml:68 formcard/labs/AboutKDE.qml:78
#, kde-format
msgid ""
"KDE is a world-wide community of software engineers, artists, writers, "
"translators and creators who are committed to Free Software development. KDE "
"produces the Plasma desktop environment, hundreds of applications, and the "
"many software libraries that support them.\n"
"\n"
"KDE is a cooperative enterprise: no single entity controls its direction or "
"products. Instead, we work together to achieve the common goal of building "
"the world's finest Free Software. Everyone is welcome to join and contribute "
"to KDE, including you."
msgstr ""
"O KDE é uma comunidade mundial de engenheiros informáticos, artistas, "
"escritores, tradutores e criadores que estão comprometidos com o "
"desenvolvimento de 'software' livre. O KDE produz o ambiente de trabalho "
"Plasma, centenas de aplicações e uma enorme quantidade bibliotecas de "
"'software' que dão suporte às mesmas\n"
"\n"
"O KDE é uma empresa corporativa: nenhuma entidade única controla a sua "
"direcção ou produtos. Em vez disso, trabalhamos em conjunto para obter o "
"objectivo comum de criar o melhor Software Livre do mundo. Todos são bem-"
"vindos para se juntarem e contribuírem para o KDE, incluindo-o a si."

#: formcard/AboutKDE.qml:76 formcard/AboutPage.qml:179
#: formcard/labs/AboutKDE.qml:86 formcard/labs/AboutPage.qml:194
#, kde-format
msgid "Homepage"
msgstr "Página Web"

#: formcard/AboutKDE.qml:82
#, fuzzy, kde-format
#| msgid "Report a bug"
msgid "Report bugs"
msgstr "Comunicar um erro"

#: formcard/AboutKDE.qml:87 formcard/labs/AboutKDE.qml:99
#, kde-format
msgid ""
"Software can always be improved, and the KDE team is ready to do so. "
"However, you - the user - must tell us when something does not work as "
"expected or could be done better.\n"
"\n"
"KDE has a bug tracking system. Use the button below to file a bug, or use "
"the program's About page to report a bug specific to this application.\n"
"\n"
"If you have a suggestion for improvement then you are welcome to use the bug "
"tracking system to register your wish. Make sure you use the severity called "
"\"Wishlist\"."
msgstr ""
"As aplicações podem sempre ser melhoradas, e a equipa do KDE está disponível "
"para fazê-lo. Contudo você - o utilizador - deve-nos indicar quando algo não "
"faz o que seria esperado ou que poderia ser feito de melhor forma.\n"
"\n"
"O KDE tem um sistema de registo de erros. Use o botão abaixo para comunicar "
"um erro ou use a página 'Acerca' do programa para comunicar um erro "
"específico desta aplicação.\n"
"\n"
"Se tiver uma sugestão de melhoria, então é bem-vindo para usar o sistema de "
"registo de erros para registar o seu pedido. Certifique-se que usa o nível "
"de criticidade \"Pedido\"."

#: formcard/AboutKDE.qml:96 formcard/AboutPage.qml:230
#: formcard/labs/AboutKDE.qml:108 formcard/labs/AboutPage.qml:248
#, kde-format
msgid "Report a bug"
msgstr "Comunicar um erro"

#: formcard/AboutKDE.qml:102
#, kde-format
msgid "Join us"
msgstr ""

#: formcard/AboutKDE.qml:107 formcard/labs/AboutKDE.qml:121
#, kde-format
msgid ""
"You do not have to be a software developer to be a member of the KDE team. "
"You can join the national teams that translate program interfaces. You can "
"provide graphics, themes, sounds, and improved documentation. You decide!"
msgstr ""
"Não tem de ser um programador de aplicações para ser um membro da equipa do "
"KDE. Pode-se juntar às equipas nacionais que traduzem as interfaces dos "
"programas. Poderá fornecer gráficos, temas, sons e melhoras na documentação. "
"Você decide!"

#: formcard/AboutKDE.qml:115 formcard/AboutPage.qml:205
#: formcard/labs/AboutKDE.qml:128 formcard/labs/AboutPage.qml:222
#, kde-format
msgid "Get Involved"
msgstr "Envolva-se"

#: formcard/AboutKDE.qml:123 formcard/labs/AboutKDE.qml:135
#, kde-format
msgid "Developer Documentation"
msgstr "Documentação de Desenvolvimento"

#: formcard/AboutKDE.qml:129
#, kde-format
msgid "Support us"
msgstr ""

#: formcard/AboutKDE.qml:134 formcard/labs/AboutKDE.qml:148
#, kde-format
msgid ""
"KDE software is and will always be available free of charge, however "
"creating it is not free.\n"
"\n"
"To support development the KDE community has formed the KDE e.V., a non-"
"profit organization legally founded in Germany. KDE e.V. represents the KDE "
"community in legal and financial matters.\n"
"\n"
"KDE benefits from many kinds of contributions, including financial. We use "
"the funds to reimburse members and others for expenses they incur when "
"contributing. Further funds are used for legal support and organizing "
"conferences and meetings.\n"
"\n"
"We would like to encourage you to support our efforts with a financial "
"donation.\n"
"\n"
"Thank you very much in advance for your support."
msgstr ""
"As aplicações do KDE são e sempre serão disponíveis sem quaisquer encargos; "
"contudo, a sua criação não é gratuita.\n"
"\n"
"Para apoiar o desenvolvimento, a comunidade do KDE criou o KDE e.V., uma "
"organização sem fins lucrativos, criada legalmente na Alemanha. O KDE e.V. "
"representa a comunidade do KDE a nível legal e financeiro.\n"
"\n"
"O KDE benefiicia com vários tipos de contribuições, incluindo as "
"financeiras. Usamos o financiamento para reembolsar os membros e outras "
"pessoas pelas despesas na contribuição. Alguns dos fundos são usados para "
"apoio legal e para organizar conferências e reuniões.\n"
"\n"
"Gostaríamos de o encorajar a apoiar os nossos esforços com uma doação "
"financeira.\n"
"\n"
"Desde já, muito obrigado pelo seu apoio."

#: formcard/AboutKDE.qml:146 formcard/labs/AboutKDE.qml:159
#, kde-format
msgid "KDE e.V"
msgstr "KDE e.V"

#: formcard/AboutKDE.qml:154 formcard/AboutPage.qml:192
#: formcard/labs/AboutKDE.qml:166 formcard/labs/AboutPage.qml:208
#, kde-format
msgid "Donate"
msgstr "Doar"

#: formcard/AboutPage.qml:87 formcard/labs/AboutPage.qml:96
#, kde-format
msgid "About %1"
msgstr "Acerca do %1"

#: formcard/AboutPage.qml:133 formcard/labs/AboutPage.qml:148
#, kde-format
msgid "Copyright"
msgstr "Copyright"

#: formcard/AboutPage.qml:141 formcard/labs/AboutPage.qml:155
#, kde-format
msgid "License"
msgid_plural "Licenses"
msgstr[0] "Licença"
msgstr[1] "Licenças"

#: formcard/AboutPage.qml:237 formcard/labs/AboutPage.qml:261
#, kde-format
msgid "Libraries in use"
msgstr "Bibliotecas em uso"

#: formcard/AboutPage.qml:260 formcard/labs/AboutPage.qml:289
#, kde-format
msgid "Authors"
msgstr "Autores"

#: formcard/AboutPage.qml:275 formcard/labs/AboutPage.qml:301
#, kde-format
msgid "Credits"
msgstr "Créditos"

#: formcard/AboutPage.qml:290 formcard/labs/AboutPage.qml:313
#, kde-format
msgid "Translators"
msgstr "Tradutores"

#: formcard/AboutPage.qml:359
#, kde-format
msgid "Visit %1's KDE Store page"
msgstr ""

#: formcard/AboutPage.qml:368
#, kde-format
msgid "Send an email to %1"
msgstr ""

#: settings/CategorizedSettings.qml:44
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr ""

#: settings/CategorizedSettings.qml:44
#, kde-format
msgctxt "@title:window"
msgid "Settings — %1"
msgstr ""
