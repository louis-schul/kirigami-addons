/*
 *   SPDX-FileCopyrightText: 2023 Marco MArtin <mart@kde.org>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import org.kde.plasma.core 2.0 as PlasmaCore

PlasmaCore.Svg {}
